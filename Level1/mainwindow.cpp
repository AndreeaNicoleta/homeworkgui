#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	//Level 1 a)
	int value = ui->horizontalSlider->value();

	//Level 1 b)
	QString text = ui->lineEdit->text();

	//Level 1 c)
	ui->lineEdit_2->setText("Hello, I set the text!");

	connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), ui->label, SLOT(setNum(int)));
	connect(ui->lineEdit, SIGNAL(textChanged(QString)), ui->label_2, SLOT(setText(QString)));

	
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setNum(int value)
{
	ui->label->setText("" + value);
}
void MainWindow::setText(QString text)
{
	ui->label_2->setText(text);
	
}