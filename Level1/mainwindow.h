#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qlabel.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
	public slots:
	void setNum(int value);
	void setText(QString text);
		


private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
