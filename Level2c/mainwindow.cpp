#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qlineedit.h>
#include <qpushbutton.h>
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	QPushButton * pushButton = new QPushButton(ui->centralWidget);
	connect(pushButton, SIGNAL(clicked()), this, SLOT(addLineEdit()));

	QVBoxLayout * layout = new QVBoxLayout;
	layout->addWidget(pushButton);
	ui->centralWidget->setLayout(layout);

	pushButton->setText("Add a new line edit");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addLineEdit()
{
	QLineEdit * otherLineEdit = new QLineEdit(ui->centralWidget);
	otherLineEdit->setText("I was added");

	ui->centralWidget->layout()->addWidget(otherLineEdit);
}