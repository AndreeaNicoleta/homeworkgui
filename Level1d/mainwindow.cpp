#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPlainTextEdit>
#include <QVBoxLayout> 
#include <QFileDialog>
#include <fstream>
#include <QFileDialog>
#include <QFile>
#include <QString>
#include <QTextStream>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	editor = new QPlainTextEdit(ui->centralWidget);
	QVBoxLayout *layout = new QVBoxLayout;

	layout->addWidget(editor);
	ui->centralWidget->setLayout(layout);

	connect(ui->actionOpen, SIGNAL(triggered(bool)), this, SLOT(openFile()));

	
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFile()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "C://", "All files (*.*);;Text File (*.txt)");
	
	if (fileName.isEmpty())
		return;

	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;

	QTextStream in(&file);
	while (!in.atEnd()) {
		QString line = in.readLine();
		editor->appendPlainText(line);
	}
	




}