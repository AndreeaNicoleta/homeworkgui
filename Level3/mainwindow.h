#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTabWidget>
#include <qplaintextedit.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
	void saveAsFile();
	void newFile();
	void saveFile();
	void openFile();

private:
    Ui::MainWindow *ui;
	QTabWidget * editor;
	QList<QPlainTextEdit *> allTextEdits;
	QList<QString> allFiles;
};

#endif // MAINWINDOW_H
