/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QWidget *displayWidget;
    QSlider *redSlider;
    QLabel *label_3;
    QSpinBox *blueSpinBox;
    QSpinBox *greenSpinBox;
    QSlider *blueSlider;
    QSlider *greenSlider;
    QSpinBox *redSpinBox;
    QLabel *label;
    QLabel *label_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(400, 300);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        displayWidget = new QWidget(centralWidget);
        displayWidget->setObjectName(QStringLiteral("displayWidget"));
        displayWidget->setMinimumSize(QSize(64, 64));
        displayWidget->setAutoFillBackground(true);
        redSlider = new QSlider(displayWidget);
        redSlider->setObjectName(QStringLiteral("redSlider"));
        redSlider->setGeometry(QRect(81, 34, 212, 22));
        redSlider->setMaximum(255);
        redSlider->setOrientation(Qt::Horizontal);
        label_3 = new QLabel(displayWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(30, 166, 45, 23));
        QFont font;
        font.setFamily(QStringLiteral("Comic Sans MS"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label_3->setFont(font);
        blueSpinBox = new QSpinBox(displayWidget);
        blueSpinBox->setObjectName(QStringLiteral("blueSpinBox"));
        blueSpinBox->setGeometry(QRect(299, 167, 43, 20));
        blueSpinBox->setMaximum(255);
        greenSpinBox = new QSpinBox(displayWidget);
        greenSpinBox->setObjectName(QStringLiteral("greenSpinBox"));
        greenSpinBox->setGeometry(QRect(299, 101, 43, 20));
        greenSpinBox->setMaximum(255);
        blueSlider = new QSlider(displayWidget);
        blueSlider->setObjectName(QStringLiteral("blueSlider"));
        blueSlider->setGeometry(QRect(81, 166, 212, 22));
        blueSlider->setMaximum(255);
        blueSlider->setOrientation(Qt::Horizontal);
        greenSlider = new QSlider(displayWidget);
        greenSlider->setObjectName(QStringLiteral("greenSlider"));
        greenSlider->setGeometry(QRect(81, 100, 212, 22));
        greenSlider->setMaximum(255);
        greenSlider->setOrientation(Qt::Horizontal);
        redSpinBox = new QSpinBox(displayWidget);
        redSpinBox->setObjectName(QStringLiteral("redSpinBox"));
        redSpinBox->setGeometry(QRect(299, 35, 43, 20));
        redSpinBox->setMaximum(255);
        label = new QLabel(displayWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 34, 45, 23));
        label->setFont(font);
        label_2 = new QLabel(displayWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 100, 45, 23));
        label_2->setFont(font);

        gridLayout->addWidget(displayWidget, 0, 0, 2, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);
        QObject::connect(redSlider, SIGNAL(valueChanged(int)), redSpinBox, SLOT(setValue(int)));
        QObject::connect(redSpinBox, SIGNAL(valueChanged(int)), redSlider, SLOT(setValue(int)));
        QObject::connect(greenSlider, SIGNAL(valueChanged(int)), greenSpinBox, SLOT(setValue(int)));
        QObject::connect(greenSpinBox, SIGNAL(valueChanged(int)), greenSlider, SLOT(setValue(int)));
        QObject::connect(blueSlider, SIGNAL(valueChanged(int)), blueSpinBox, SLOT(setValue(int)));
        QObject::connect(blueSpinBox, SIGNAL(valueChanged(int)), blueSlider, SLOT(setValue(int)));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Blue", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Red", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Green", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
