#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPalette>
#include <qwindow.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	connect(ui->redSlider, SIGNAL(valueChanged(int)), SLOT(onColorChanged()));
	connect(ui->greenSlider, SIGNAL(valueChanged(int)), SLOT(onColorChanged()));
	connect(ui->blueSlider, SIGNAL(valueChanged(int)), SLOT(onColorChanged()));
	onColorChanged();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onColorChanged()
{
	m_color.setRgb(ui->redSlider->value(), ui->greenSlider->value(), ui->blueSlider->value());
	QPalette pal = ui->displayWidget->palette();
	pal.setColor(QPalette::Window, m_color);
	ui->displayWidget->setPalette(pal);
	
	

}