#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QColor>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT
	//Q_PROPERTY(QColor color READ color NOTIFY onColorChanged)

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
	void onColorChanged();

private:
    Ui::MainWindow *ui;
	QColor m_color;
};

#endif // MAINWINDOW_H
